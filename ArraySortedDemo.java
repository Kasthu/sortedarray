package sorted.lk;

public class ArraySortedDemo {
	public static void main(String args[]) {
		int array[] = { 10, 20, 5, 15, 30, 25 };
		ArraySorted sort = new ArraySorted();
		System.out.println(sort.findmin(array));
		System.out.println(sort.findminindex(array));
		System.out.print("unsorted order: ");
		for (int i = 0; i < array.length; i++) {

			System.out.print(+array[i] + "  ");
		}
		System.out.println();

		sort.findsorted(array);
		System.out.print("sorted order  : ");
		for (int i = 0; i < array.length; i++) {

			System.out.print(+array[i] + "  ");
		}

	}
}
